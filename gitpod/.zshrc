# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set the theme you want to use
ZSH_THEME="robbyrussell"

# Enable the plugins you want
plugins=(
  colored-man-pages
  colorize
  docker
  terraform
  git
  kubectl
  zsh-autosuggestions
  zsh-syntax-highlighting
)

source $ZSH/oh-my-zsh.sh

# Completions

## setup for zsh via https://github.com/zsh-users/zsh-completions
# git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:-${ZSH:-~/.oh-my-zsh}/custom}/plugins/zsh-completions
fpath+=${ZSH_CUSTOM:-${ZSH:-~/.oh-my-zsh}/custom}/plugins/zsh-completions/src

# GO Task Setup
mkdir -p ~/.oh-my-zsh/completions
fpath=(~/.oh-my-zsh/completions $fpath)
autoload -Uz compinit && compinit

unalias gp

# You can add other configurations below this comment

## LocalStack
localstack completion zsh > "${fpath[1]}/_localstack"
# export CONFIG_DIR=/workspace/gitpod-demo/.localstack
