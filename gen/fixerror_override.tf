resource "aws_ecs_task_definition" "my_task" {
  family                   = "my-family"
  network_mode             = "bridge"
  requires_compatibilities = ["EC2"]
  cpu                      = "256"
  memory                   = "512"
  container_definitions    = jsonencode([{
    name  = "my-container"
    image = "nginx"
    memory = 100
    ports = [{
      containerPort = 80
      hostport = 8080
    }]
  }])
}
