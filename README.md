
[![Open in Gitpod](https://gitpod-staging.com/button/open-in-gitpod.svg 'Opens this repo in gitpod')](https://gitpod.io/#https://gitlab.com/ziyotek3/containers/lab-building_image-todo_app/lab-building_image-todo_app/-/tree/main/)
## README

# Docker Lab Automation Tasks

This document provides an overview of the Taskfile commands available to manage, test, and validate Docker-based lab activities. These tasks are designed to help in setting up a Docker environment, performing specific lab steps, and verifying those steps.

## Prerequisites
- Docker
- [Taskfile](https://taskfile.dev)
- A GitPod environment (for some commands)

## Available Tasks

### Docker Management (taskfiles/docker.yml)

1. **Reset Docker to a clean state**
    ```bash
    task docker:reset
    ```

2. **Check if Docker daemon is running**
    ```bash
    task docker:is-daemon-running
    ```

### Lab Activities (taskfiles/lab.yml)

1. **Setup initial lab requirements**
    ```bash
    task lab:initial
    ```

2. **Build the 'getting-started' Docker image**
    ```bash
    task lab:build-image
    ```

3. **Run the 'getting-started' Docker container**
    ```bash
    task lab:run-container
    ```

4. **Prepare instruction slides with revealjs**
    ```bash
    task lab:prepare-instructions-slides
    ```

### Lab Validations (taskfiles/validation.yml)

1. **Validate if Docker daemon is running**
    ```bash
    task validation:validate-docker
    ```

2. **Check if the Docker container is running correctly**
    ```bash
    task validation:validate-container-running
    ```

3. **Validate HTTP 200 response from the running container**
    ```bash
    task validation:validate-http-response
    ```

4. **Ensure the 'getting-started' image is built**
    ```bash
    task validation:validate-image-built
    ```

### Main Taskfile.yml

1. **Open the project in a web browser**
    ```bash
    task preview
    ```

2. **Reset the workspace to its starting point without altering the code**
    ```bash
    task reset
    ```

3. **Validate the readiness of the lab GitPod workspace**
    ```bash
    task validate-lab-start
    ```

4. **Execute lab step 1: Building the Docker Image**
    ```bash
    task perform-lab-step-1
    ```

5. **Validate lab step 1 for Docker image existence**
    ```bash
    task validate-lab-step-1
    ```

6. **Test the entire lab step 1**
    ```bash
    task test-lab-step-1
    ```

7. **Execute lab step 2: Running the Docker Container**
    ```bash
    task perform-lab-step-2
    ```

8. **Validate lab step 2 for Docker container execution**
    ```bash
    task validate-lab-step-2
    ```

9. **Test the entire lab step 2**
    ```bash
    task test-lab-step-2
    ```

10. **Validate the completion of all lab steps**
    ```bash
    task validate-lab-final
    ```

11. **Full test for all lab steps**
    ```bash
    task test-lab-full
    ```

## Usage

1. Clone this repository to your local machine.
2. Navigate to the root directory where `Taskfile.yml` resides.
3. Run the desired task using the `task <task-name>` command.

## Contribute

Contributions are welcome! Feel free to raise an issue or submit a pull request.

## License

This project is under the MIT License. Refer to the LICENSE file for more details.
