# Start with Gitpod's base image
FROM gitpod/workspace-full

# Install Docker (Docker is already included in Gitpod's base image, but just in case you want to have the latest)
RUN sudo apt-get update && \
    sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common && \
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - && \
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" && \
    sudo apt-get update && \
    sudo apt-get install -y docker-ce

# Grant the gitpod user permission to run Docker
RUN sudo adduser gitpod docker

# Install Go-task
RUN brew install go-task

# Install Goss
RUN \
    sudo curl -L https://github.com/goss-org/goss/releases/latest/download/goss-linux-amd64 -o /usr/local/bin/goss && \
    sudo chmod +rx /usr/local/bin/goss && \
    sudo curl -L https://github.com/goss-org/goss/releases/latest/download/dgoss -o /usr/local/bin/dgoss && \
    sudo chmod +rx /usr/local/bin/dgoss

# Install ZSH and oh-my-zsh
RUN brew install zsh && \
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

COPY gitpod/.zshrc /home/gitpod/

# Install ZSH plugins
RUN git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions && \
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

RUN git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:-${ZSH:-~/.oh-my-zsh}/custom}/plugins/zsh-completions

# Set up go-task autocompletion for zsh
RUN mkdir -p ~/.oh-my-zsh/completions && \
    wget -O ~/.oh-my-zsh/completions/_task https://raw.githubusercontent.com/go-task/task/main/completion/zsh/_task

## Add the following to your .zshrc
### fpath=(~/.oh-my-zsh/completions $fpath)
### autoload -Uz compinit && compinit

RUN brew install tree figlet terraform localstack

## Localstack completion
# put the following in your .zshrc `localstack completion zsh > "${fpath[1]}/_localstack`

# Set the default shell to zsh
ENV SHELL /bin/zsh
